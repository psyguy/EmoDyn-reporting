--- 
title: "In Search of Lost Emotions"
author: "MohammadHossein Manuel Haqiqatkhah"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
bibliography: [packages.bib] #book.bib
biblio-style: apalike
link-citations: yes
github-repo: rstudio/bookdown-demo
description: "This is a minimal example of using the bookdown package to write a book. The output format for this example is bookdown::gitbook."
---

# Preface

This document is the report of my internship at KU Leuven's Quantitative Psychology and Individual Differences Research Group in the academic year 2018-2019.

I start Chapter \@ref(intro) with an introduction and elaborate the research questions, methods, research material, and a short overwies of the following chapters. I also discuss how the goal of the internship evolved over time and how it differed from the initial intentions.
Then I move on to Chapter \@ref(pca) the first round of analyses, namely principal component analysis (PCA), and report the results.
Later in Chapter \@ref(irt) I introduce item response theory (IRT) and discuss the outcomes of the premilinary explorations via IRT.
Then, Chapter \@ref(mirt) addresses multi-dimensional IRT modeling which yields to multi-dimensional constructs for emotions.
In Chapter \@ref(fa) I apply exploratory-confirmatory factor analysis (E/CFA) and put th
In Chapter \@ref(sem) I model measurement errors and 
...

In Appendix \@ref(esmclass) appendices I include the reproducible R scripts used for this project.

